SDIR  = ./src/
IDIR  = ./statistics/
LDIR  = ./bin/
UDIR  = ./util/

MAKEFLAGS = --no-print-directory -r -s
INCLUDE = $(shell root-config --cflags)
LIBS    = $(shell root-config --libs) -lHistFactory -lXMLParser -lRooStats -lRooFit -lRooFitCore -lThread -lMinuit -lFoam

BINS = test
OBJS =

all: $(BINS)

$(BINS): % :  $(UDIR)TestCode.cxx
	@echo "Building $@ ... "
	mkdir -p $(LDIR)
	$(CXX) $(CCFLAGS) $< -I$(IDIR) $(INCLUDE) $(LIBS) $(SDIR)LimitManager.cxx $(SDIR)LimitBase.cxx $(SDIR)LimitAsymptotic.cxx $(SDIR)LimitFrequentist.cxx -o "$(LDIR)TestCode.exe"
	@echo "Done"

clean:
	rm -rf bin
