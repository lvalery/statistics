#ifndef FREQUENTIST_H
#define FREQUENTIST_H

//Roostats headers
#include "RooStats/ModelConfig.h"
#include "RooStats/HypoTestInverterResult.h"

class RooWorkspace;
class RooDataSet;
class RooRealVar;
class TString;
class LimitBase;

class LimitManager {

public:
    //
    // Usual class functions
    //
    LimitManager();
    LimitManager( const LimitManager &obj );
    ~LimitManager();

    //
    // Data functions
    //
    bool Init();
    void GetLimit();

    //
    // Setter functions
    //
    inline void rootFileName ( const TString &name ){ m_rootFileName = name; }
    inline void wsName ( const TString &name ){ m_wsName = name; }
    inline void useAsymp ( const bool use ){ m_useAsymp = use; }
    inline void mcName ( const TString &name ){ m_mcName = name; }
    inline void dataName ( const TString &name ){ m_dataName = name; }
    inline void poiScan ( const int N, const double min = -1, const double max = -1){ m_poi_scanN = N; m_poi_scanMin = min; m_poi_scanMax = max; }
    inline void setNworkers ( const int N ) { m_nWorkers = N; }
    inline void setNtoys( const int N ) { m_nToys = N; }

private:
    TString m_rootFileName, m_wsName, m_mcName, m_dataName;
    TFile *m_rootFile;
    RooWorkspace* m_ws;
    RooStats::ModelConfig *m_mc;
    RooDataSet *m_data;
    RooRealVar *m_poi;
    double m_poi_scanMax,m_poi_scanMin;
    int m_poi_scanN,m_nToys,m_nWorkers;
    RooStats::HypoTestInverterResult* m_hypoTestInvResult;
    bool m_useAsymp;
    LimitBase *m_limit;
};

#endif // BAYESIANMCMC_H
