#include <iostream>

namespace MsgStream {
    const std::string ERROR = "\033[1;31m";
    const std::string WARNING = "\033[1;33m";
    const std::string STD = "\033[0m";
}
