#ifndef _LIMITBASE_H_
#define _LIMITBASE_H_

//Roostats headers
#include "RooStats/ModelConfig.h"
#include "RooStats/HypoTestInverterResult.h"

//Roostats headers
#include "RooFitResult.h"

class RooAbsPdf;
class RooDataSet;
class RooWorkspace;

class LimitBase {
    
public:
    
    //
    // Struct for exporting limit results
    //
    struct LimitResult {
        double ExpMed   = 0.;
        double Obs      = 0.;
        double ExpM1s   = 0.;
        double ExpM2s   = 0.;
        double ExpP1s   = 0.;
        double ExpP2s   = 0.;
    };
    
    //
    // Standard functions
    //
    LimitBase();
    LimitBase( const LimitBase &);
    ~LimitBase();
    
    //
    // Setter functions
    //
    inline void PoiScan ( const int N, const double min = -1, const double max = -1){ m_poi_scanN = N; m_poi_scanMin = min; m_poi_scanMax = max; }
    inline void SetMsgLevel( const int level) { m_msgLevel = level; }
    inline void SetNWorkers( const int N ){ m_nWorkers = N; }
    inline void SetNToys( const int N ){ m_nToys = N; }
    
    //
    // Class-specific functions
    //
    virtual LimitBase::LimitResult GetLimit( RooWorkspace *ws, RooStats::ModelConfig *mc, RooDataSet *data, RooRealVar* mu );
    void PrintResults();
    virtual void DrawResults();
    
protected:
    double m_poi_scanMax,m_poi_scanMin;
    int m_poi_scanN, m_msgLevel, m_nWorkers, m_nToys;
    RooStats::HypoTestInverterResult* m_pHTIR;
    
};

#endif //LIMITBASE_H