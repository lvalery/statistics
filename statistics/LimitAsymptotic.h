#ifndef LIMITASYMPTOTIC_H
#define LIMITASYMPTOTIC_H

#include "LimitBase.h"

class RooAbsPdf;
class RooDataSet;
class RooWorkspace;

class LimitAsymptotic : public LimitBase {

public:

    //
    // Standard functions
    //
    LimitAsymptotic();
    LimitAsymptotic( const LimitAsymptotic &);
    ~LimitAsymptotic();

    //
    // Class-specific functions
    //
    virtual LimitBase::LimitResult GetLimit( RooWorkspace *ws, RooStats::ModelConfig *mc, RooDataSet *data, RooRealVar* mu );
    virtual void DrawResults();

};

#endif //LIMITASYMPTOTIC_H
