#ifndef _LIMITFREQUENTIST_H_
#define _LIMITFREQUENTIST_H_

#include "LimitBase.h"

class RooAbsPdf;
class RooDataSet;

class LimitFrequentist : public LimitBase {

public:

    //
    // Standard functions
    //
    LimitFrequentist();
    LimitFrequentist( const LimitFrequentist &);
    ~LimitFrequentist();

    //
    // Class-specific functions
    //
    virtual LimitBase::LimitResult GetLimit( RooWorkspace *ws, RooStats::ModelConfig *mc, RooDataSet *data, RooRealVar* mu );
    virtual void DrawResults();

};

#endif //_LIMITFREQUENTIST_H_
