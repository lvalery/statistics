
#include "LimitManager.h"
using namespace RooStats ;

int main()
{
    LimitManager f;
    f.rootFileName("example/DDbkg_AllSyst_SRVLQ7_DDbkg_AllSyst_model_Normal.root");
    f.wsName("SRVLQ7");
    f.mcName("ModelConfig");
    f.dataName("obsData");
    f.useAsymp(false);
    f.poiScan(2,0.,0.5);

    //use Nworkers=0 for disabling prooflite
    f.setNworkers(0);
    //use Nworkers>0 for enabling prooflite
    f.setNworkers(2);

    f.setNtoys(1000);
    f.Init();
    f.GetLimit();
    return 1;
}
