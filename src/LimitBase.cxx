//Standard C++ headers
#include <iostream>

//Roofit headers
#include "RooDataSet.h"
#include "RooRealVar.h"

//Corresponding header
#include "LimitBase.h"

using namespace RooStats;
using namespace RooFit;
using namespace std;

//________________________________________________________________________
//
LimitBase::LimitBase():
m_poi_scanMin(-1),
m_poi_scanMax(-1),
m_poi_scanN(-1),
m_msgLevel(1),
m_nWorkers(-1),
m_nToys(-1),
m_pHTIR(0)
{}

//________________________________________________________________________
//
LimitBase::~LimitBase()
{}

//________________________________________________________________________
//
LimitBase::LimitBase( const LimitBase &q ){
    m_poi_scanMin   = q.m_poi_scanMin;
    m_poi_scanMax   = q.m_poi_scanMax;
    m_poi_scanN     = q.m_poi_scanN;
    m_msgLevel      = q.m_msgLevel;
    m_pHTIR         = q.m_pHTIR;
    m_nWorkers      = q.m_nWorkers;
    m_nToys         = q.m_nToys;
}

//________________________________________________________________________
//
LimitBase::LimitResult LimitBase::GetLimit( RooWorkspace *ws, ModelConfig *mc, RooDataSet *data, RooRealVar* mu ){
    std::cout << "In LimitBase::GetLimit() => You should not be there !" << std::endl;
    LimitResult res;
    return res;
}

//________________________________________________________________________
//
void LimitBase::DrawResults() {
    std::cout << "In LimitBase::DrawResults() => You should not be there !" << std::endl;
}

//________________________________________________________________________
//
void LimitBase::PrintResults() {

    //
    // Sanity check: verify that the pointer exists
    //
    if (!m_pHTIR) {
        std::cerr << "<!> ERROR in LimitBase::PrintResults(): m_pHTIR is not defined ! Please check." << std::endl;
        return;
    }

    //
    // Prints the results on the screeen
    //
    const double obsLimit = m_pHTIR -> UpperLimit();
    const double expLimit = m_pHTIR -> GetExpectedUpperLimit(0);
    const double expLimit_m1s = m_pHTIR -> GetExpectedUpperLimit(-1);
    const double expLimit_m2s = m_pHTIR -> GetExpectedUpperLimit(-2);
    const double expLimit_p1s = m_pHTIR -> GetExpectedUpperLimit(1);
    const double expLimit_p2s = m_pHTIR -> GetExpectedUpperLimit(2);

    std::cout << "================================" << std::endl;
    std::cout << "expLimit = " << expLimit << std::endl;
    std::cout << "obsLimit = " << obsLimit << std::endl;
    std::cout << "expLimit_m1s = " << expLimit_m1s << std::endl;
    std::cout << "expLimit_m2s = " << expLimit_m2s << std::endl;
    std::cout << "expLimit_p1s = " << expLimit_p1s << std::endl;
    std::cout << "expLimit_p2s = " << expLimit_p2s << std::endl;
    std::cout << "================================" << std::endl;
}
