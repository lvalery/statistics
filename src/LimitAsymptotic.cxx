#include <iostream>

//Root headers
#include "TFile.h"
#include "TCanvas.h"

//Roostats headers
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/AsymptoticCalculator.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestPlot.h"
#include "RooStats/HypoTestInverterPlot.h"

//Roofit headers
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooRealVar.h"

#include "LimitAsymptotic.h"

#include "Common.h"

using namespace RooStats;
using namespace RooFit;
using namespace std;


//________________________________________________________________________
//
LimitAsymptotic::LimitAsymptotic():
LimitBase()
{}

//________________________________________________________________________
//
LimitAsymptotic::~LimitAsymptotic()
{}

//________________________________________________________________________
//
LimitAsymptotic::LimitAsymptotic( const LimitAsymptotic &q ):
LimitBase(q)
{}

//________________________________________________________________________
//
LimitBase::LimitResult LimitAsymptotic::GetLimit( RooWorkspace *ws, ModelConfig *mc, RooDataSet *data, RooRealVar* mu ){

    LimitBase::LimitResult res;

    RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING) ;

    if ( !mc->GetSnapshot() ) {
        cout << "-> Creating snapshot for ModelConfig" << endl;
        mc->SetSnapshot(*mc->GetParametersOfInterest());
    }

    //
    // 1) Creating the models (it is assumed that mc is the s+b hypothesis)
    //

    //---
    // 1a) Creating the b-only model
    //---
    ModelConfig* mcbOnly = (ModelConfig*) mc->Clone();
    TString mcbOnly_Name("ModelConfig_bOnly");
    mcbOnly->SetName(mcbOnly_Name);
    RooRealVar *var = dynamic_cast<RooRealVar*>(mcbOnly->GetParametersOfInterest()->first());
    if(var) {
        double oldval = var->getVal();
        var->setVal(0);
        mcbOnly->SetSnapshot(RooArgSet(*var));// records ModelConfig with the mu=0 configuration in a SnapShot
        var->setVal(oldval);// the ModelConfig is set back to mu=oldval
    } else {
        throw runtime_error("ERROR !");
        cout << "ERROR ! parameter of interest for ModelConfig_bOnly not found" <<endl;
        return res;
    }

    if ( !mcbOnly->GetSnapshot() ) { //the b-only SnapShot has not been properly set :-(
        cout << "-> Creating snapshot for ModelConfig_bOnly" << endl;
        double oldval = var->getVal();
        if(oldval!=0) {
            throw runtime_error("ERROR !");
            cout << MsgStream::ERROR << "ERROR ! mu should be 0 but it's not" << MsgStream::STD << endl;
            return res;
        }
        var->setVal(0);
        mcbOnly->SetSnapshot(RooArgSet(*var));
        var->setVal(oldval);
    }

    //
    // To be safe, increasing the allowed value of mu
    //
    mu -> setRange(0,2000);

    //
    // 2) Test statistics definition
    //
    ProfileLikelihoodTestStat profll(*mc->GetPdf()); //only this test statistics allows the asymptotic mode
    profll.SetOneSided(1);//limits configuration (q_mu)
    profll.SetReuseNLL(false);

    TestStatistic* testStat = &profll;

    //
    // 3) Defines the calculator (asymptotic)
    //
    AsymptoticCalculator *hypoCalc = new AsymptoticCalculator(*data, *mcbOnly, *mc);//data, alternate, null ==> Here, null is the s+b model (limit configuration)
    hypoCalc -> SetPrintLevel(m_msgLevel);
    hypoCalc -> SetOneSided(true);

    //
    // 4) Uses the HypoTestInverter class to get the confidence interval
    //
    HypoTestInverter calc(*hypoCalc);
    calc.SetVerbose(0);
    calc.SetConfidenceLevel(0.95);
    calc.UseCLs(true);
    if(m_poi_scanN>-1){
      calc.SetFixedScan(m_poi_scanN,m_poi_scanMin,m_poi_scanMax);
    } else {
      calc.SetAutoScan();
    }

    //
    // 5) Compute and save the limit result into the HypoTestInverterResult object
    //
    m_pHTIR = calc.GetInterval();

    res.Obs    = m_pHTIR -> UpperLimit();
    res.ExpMed = m_pHTIR -> GetExpectedUpperLimit(0);
    res.ExpM1s = m_pHTIR -> GetExpectedUpperLimit(-1);
    res.ExpM2s = m_pHTIR -> GetExpectedUpperLimit(-2);
    res.ExpP1s = m_pHTIR -> GetExpectedUpperLimit(1);
    res.ExpP2s = m_pHTIR -> GetExpectedUpperLimit(2);

    return res;

}

//________________________________________________________________________
//
void LimitAsymptotic::DrawResults() {

    if(!m_pHTIR){
        std::cerr << MsgStream::ERROR << "<!> ERROR in LimitAsymptotic::DrawResults(): the HypoTestInvertorResult is NULL. Please check !" << MsgStream::STD << std::endl;
        return;
    }

    //
    // 1) Create an output file to store the result
    //
    TFile* fileOut = new TFile("outputAsymptotic.root","RECREATE");
    m_pHTIR->Write();

    //
    // 2) Creates the nice plot CLs/b/sb vs mu
    //
    TCanvas* c1 = new TCanvas("c1","CL vs mu",400,400);
    HypoTestInverterPlot *plot = new HypoTestInverterPlot("HTI_Result_Plot","CL scan",m_pHTIR);
    plot->Draw("CLb 2CL");  // plot all and Clb
    c1->Write();
    c1->Print("AsymptoticResult.eps");
    fileOut->Close();

    delete c1;
    delete plot;
}
