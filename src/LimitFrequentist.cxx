#include <iostream>

//Root headers
#include "TFile.h"
#include "TCanvas.h"

//Roostats headers
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestPlot.h"
#include "RooStats/HypoTestInverterPlot.h"
#include "RooStats/FrequentistCalculator.h"
#include "RooStats/ToyMCSampler.h"

//Roofit headers
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooRealVar.h"

#include "LimitFrequentist.h"
#include "Common.h"


//________________________________________________________________________
//
LimitFrequentist::LimitFrequentist():
LimitBase()
{}

//________________________________________________________________________
//
LimitFrequentist::~LimitFrequentist()
{}

//________________________________________________________________________
//
LimitFrequentist::LimitFrequentist( const LimitFrequentist &q ):
LimitBase(q)
{}

//________________________________________________________________________
//
LimitBase::LimitResult LimitFrequentist::GetLimit( RooWorkspace *ws, RooStats::ModelConfig *mc, RooDataSet *data, RooRealVar* mu ){

    LimitBase::LimitResult res;

    //
    // Sanity checks (chek that the data memebers are set correctly)
    //
    if( m_nToys==-1 ){
        std::cerr << MsgStream::ERROR << "<!> ERROR: please set the number of toys to use by calling the function LimitBase::SetNToys(N)." << MsgStream::STD << std::endl;
        return res;
    }

    RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING) ;

    if (!mc->GetSnapshot() ) {
        std::cout << "-> Creating snapshot for ModelConfig" << std::endl;
        mc->SetSnapshot(*mc->GetParametersOfInterest());
    }

    //
    // 1) Creating the models (it is assumed that mc is the s+b hypothesis)
    //

    //---
    // 1a) Creating the b-only model
    //---
    std::cout << "-> Creating the b-only model" << std::endl;
    RooStats::ModelConfig* mcbOnly = (RooStats::ModelConfig*) mc->Clone();
    TString mcbOnly_Name("ModelConfig_bOnly");
    mcbOnly->SetName(mcbOnly_Name);
    RooRealVar *var = dynamic_cast<RooRealVar*>(mcbOnly->GetParametersOfInterest()->first());
    if(var) {
        double oldval = var->getVal();
        var->setVal(0);
        mcbOnly->SetSnapshot(RooArgSet(*var));// records ModelConfig with the mu=0 configuration in a SnapShot
        var->setVal(oldval);// the ModelConfig is set back to mu=oldval
    }
    else {
        throw std::runtime_error("ERROR !");
        std::cout << MsgStream::ERROR << "ERROR ! parameter of interest for ModelConfig_bOnly not found" << MsgStream::STD << std::endl;
        return res;
    }

    if (!mcbOnly->GetSnapshot() ) { //the b-only SnapShot has not been properly set :-(
        std::cout << "-> Creating snapshot for ModelConfig_bOnly" << std::endl;
        double oldval = var->getVal();
        if(oldval!=0) {
            throw std::runtime_error("ERROR !");
            std::cout << MsgStream::ERROR << "ERROR ! mu should be 0 but it's not" << MsgStream::STD << std::endl;
            return res;
        }
        var->setVal(0);
        mcbOnly->SetSnapshot(RooArgSet(*var));
        var->setVal(oldval);
    }

    //
    // Enlarging the range of mu to be safe
    //
    std::cout << "-> Setting mu" << std::endl;
    mu->setRange(0,1000);

    //
    // 2) Test statistics definition
    //
    std::cout << "-> Defining the test statistics" << std::endl;
    RooStats::ProfileLikelihoodTestStat profll(*mc->GetPdf()); //only this test statistics allows the asymptotic mode
    profll.SetOneSided(1);//limits configuration (q_mu)
    profll.SetReuseNLL(false);

    RooStats::TestStatistic* testStat = &profll;

    //
    // 3) Defines the calculator (toys)
    //
    std::cout << "-> Defining the calculator" << std::endl;
    RooStats::FrequentistCalculator* hypoCalc = new RooStats::FrequentistCalculator(*data,*mcbOnly, *mc);//data,alt,null
    hypoCalc -> SetToys(m_nToys,m_nToys/10.);// null toys, alt toys
    RooStats::ToyMCSampler *toymcs = (RooStats::ToyMCSampler*)hypoCalc->GetTestStatSampler();
    toymcs->SetTestStatistic(testStat);
    RooStats::ProofConfig * pc = nullptr;
    if(m_nWorkers>0) {
        pc = new RooStats::ProofConfig(*ws, m_nWorkers, "", false);
        toymcs->SetProofConfig(pc); // enable proof
    }

    //
    // 4) Uses the HypoTestInverter class to get the confidence interval
    //
    std::cout << "-> Defining the HypoTestInverter" << std::endl;
    RooStats::HypoTestInverter calc(*hypoCalc);
    calc.SetVerbose(10);
    calc.SetConfidenceLevel(0.95);
    calc.UseCLs(true);
    if(m_poi_scanN>-1){
      calc.SetFixedScan(m_poi_scanN,m_poi_scanMin,m_poi_scanMax);
    }

    //
    // 5) Save the limit result into the HypoTestInverterResult object
    //
    std::cout << "-> Getting the limit" << std::endl;
    m_pHTIR = calc.GetInterval();

    res.Obs    = m_pHTIR -> UpperLimit();
    res.ExpMed = m_pHTIR -> GetExpectedUpperLimit(0);
    res.ExpM1s = m_pHTIR -> GetExpectedUpperLimit(-1);
    res.ExpM2s = m_pHTIR -> GetExpectedUpperLimit(-2);
    res.ExpP1s = m_pHTIR -> GetExpectedUpperLimit(1);
    res.ExpP2s = m_pHTIR -> GetExpectedUpperLimit(2);

    return res;

}


//________________________________________________________________________
//
void LimitFrequentist::DrawResults() {

    if(!m_pHTIR){
        std::cerr << MsgStream::ERROR << "<!> ERROR in LimitFrequentist::DrawResults(): m_pHTIR doesn't exist." << MsgStream::STD << std::endl;
        return;
    }

    TFile* fileOut = new TFile("outputFrequentist.root","RECREATE");
    m_pHTIR->Write();
    TCanvas* c1 = new TCanvas("c1","CL vs mu",400,400);
    RooStats::HypoTestInverterPlot *plot = new RooStats::HypoTestInverterPlot("HTI_Result_Plot","CL scan",m_pHTIR);
    plot->Draw("CLb 2CL");  // plot all and Clb
    c1->Write();

    const int nEntries = m_pHTIR->ArraySize();
    TCanvas* c2 = new TCanvas("c2","test stat distrib");
    c2->Divide( 2, TMath::Ceil(nEntries/2));
    for (int i=0; i<nEntries; i++) {
        c2->cd(i+1);
        RooStats::SamplingDistPlot * pl = plot->MakeTestStatPlot(i);
        pl->SetLogYaxis(true);
        pl->Draw();
    }
    fileOut->cd();
    c2 -> Write();
    c2 -> Update();
    fileOut->Close();

}
