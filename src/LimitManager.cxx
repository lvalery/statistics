//Standard C++ headers
#include <stdexcept>

//ROOT headers
#include "TFile.h"
#include "TString.h"

//Roofit headers
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooRealVar.h"

//Class header
#include "LimitManager.h"

//Other headers
#include "LimitBase.h"
#include "LimitAsymptotic.h"
#include "LimitFrequentist.h"
#include "Common.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

//_________________________________________________________________________________
//
LimitManager::LimitManager() :
m_rootFileName(""),
m_wsName("combined"),
m_mcName("ModelConfig"),
m_dataName("obsData"),
m_rootFile(0),
m_ws(0),
m_mc(0),
m_data(0),
m_poi(0),
m_poi_scanMin(-1),
m_poi_scanMax(-1),
m_poi_scanN(-1),
m_nWorkers(-1),
m_nToys(-1),
m_hypoTestInvResult(0),
m_useAsymp(false),
m_limit(0)
{}

//_________________________________________________________________________________
//
LimitManager::~LimitManager()
{}

//_________________________________________________________________________________
//
LimitManager::LimitManager( const LimitManager &q ) {
    m_rootFileName  = q.m_rootFileName;
    m_wsName        = q.m_wsName;
    m_mcName        = q.m_mcName;
    m_dataName      = q.m_dataName;
    m_rootFile      = q.m_rootFile;
    m_ws            = q.m_ws;
    m_mc            = q.m_mc;
    m_data          = q.m_data;
    m_poi           = q.m_poi;
    m_poi_scanMin   = q.m_poi_scanMin;
    m_poi_scanMax   = q.m_poi_scanMax;
    m_poi_scanN     = q.m_poi_scanN;
    m_nToys         = q.m_nToys;
    m_nWorkers      = q.m_nWorkers;
    m_hypoTestInvResult = q.m_hypoTestInvResult;
    m_useAsymp      = q.m_useAsymp;
    m_limit         = q.m_limit;
}

//_________________________________________________________________________________
//
bool LimitManager::Init() {

    //
    // Initialize the data members with what's available
    //

    std::cout << "=> Initialisating the LimitManager object. " << std::endl;

    if(m_rootFileName!="" && !m_rootFile){
        m_rootFile = new TFile(m_rootFileName,"read");
        if(!m_rootFile){
            std::cerr << MsgStream::ERROR << "<!> Error with the ROOT file ..." << MsgStream::STD << std::endl;
            return false;
        }
    }

    if(m_wsName!="" && !m_ws && m_rootFile){
        m_ws = (RooWorkspace*) m_rootFile->Get(m_wsName);
        if(!m_ws){
            std::cerr << MsgStream::ERROR << "<!> Error with the workspace ..." << MsgStream::STD << std::endl;
            return false;
        }
    }

    if(m_mcName!="" && !m_mc && m_ws){
        m_mc = (ModelConfig*)m_ws->obj(m_mcName);
        if (!m_mc) {
            cout << MsgStream::ERROR << "<!> Error with the ModelConfig object ..." << MsgStream::STD << endl;
            return false;
        }
        m_poi = (RooRealVar*)m_mc->GetParametersOfInterest()->first();
        if(m_poi_scanN>-1 && m_poi){
            if( abs(m_poi_scanMin+1)<1e-05 ) m_poi_scanMin = m_poi->getMin();
            if( abs(m_poi_scanMax+1)<1e-05 ) m_poi_scanMax = m_poi->getMax();
        }
    }

    if(m_dataName!="" && !m_data && m_ws){
        m_data = (RooDataSet*)m_ws->data(m_dataName);
        if (!m_data) {
            cout << MsgStream::ERROR << "<!> Error with the RooDataSet object ..." << MsgStream::STD << endl;
            return false;
        }
    }

    std::cout << "=> LimitManager object succesfully initialised. " << std::endl;
    return true;
}

//_________________________________________________________________________________
//
void LimitManager::GetLimit(){

    if(!m_mc || !m_data || !m_poi){
        std::cerr << MsgStream::ERROR << "<!> ERROR in LimitManager::GetLimit(): Please do Init() before. Will crash now." << MsgStream::STD << std::endl;
        abort();
    }

    //
    // 1) Declare the good limit calculator
    //

    //
    // 1.a) Asymptotic mode
    //
    if(m_useAsymp){
        m_limit = new LimitAsymptotic();
    }

    //
    // 1.b) Toys calculator
    //
    else {
        m_limit = new LimitFrequentist();
        m_limit -> SetNToys(m_nToys);
        m_limit -> SetNWorkers(m_nWorkers);
    }

    //
    // 2) Effectively computing the limits
    //
    if(m_limit){
        m_limit -> SetMsgLevel(0);
        if(m_poi_scanN>-1) m_limit -> PoiScan(m_poi_scanN,m_poi_scanMin,m_poi_scanMax);
        m_limit -> GetLimit( m_ws, m_mc, m_data, m_poi );
        m_limit -> PrintResults();
        m_limit -> DrawResults();
    } else {
        std::cerr << MsgStream::ERROR << "<!> Error in LimitManager::GetLimit(): m_limit doesn't exist" << MsgStream::STD << std::endl;
    }
}
